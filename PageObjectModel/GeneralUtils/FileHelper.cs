﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace PageObjectModel.GeneralUtils
{
    public static class FileHelper
    {
        public static string CreatePath(string FileName)
        {
            string assemblyLocation = Assembly.GetExecutingAssembly().Location;
            string projectDirectory = Path.GetDirectoryName(assemblyLocation);

            while (!File.Exists(Path.Combine(projectDirectory, FileName)))
            {
                projectDirectory = Directory.GetParent(projectDirectory).FullName;
            }

            string applicationJsonPath = Path.Combine(projectDirectory, FileName);
            return applicationJsonPath;
        }
        public static AppSettings ReadText(string FileName)
        {
            string path = CreatePath(FileName);
            var text = File.ReadAllText(path);
            var appSettings = JsonConvert.DeserializeObject<AppSettings>(text);
            return appSettings;
        }
    }
}
