﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.GeneralUtils
{
    public class AppSettings
    {
        public string Uri { get; set; }
        public string Resource { get; set; }

        public string Url { get; set; }
        public string BooksUrl { get;set; }
        public string BooksResource {  get; set; }
    }
}
