﻿using OpenQA.Selenium;
using PageObjectModel.UIFrameWork.UIUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PageObjectModel.UIFrameWork.Pages.ProfilePage
{
    public partial class Profiles
    {
        public string FindUserName(IWebDriver driver)
        {
            Waits.WaitForElementToBeVisible(driver, UserNameValue, TimeSpan.FromSeconds(10));
            IWebElement title = driver.FindElement(UserNameValue);
            string TestName = title.Text;
            return TestName;
        }
    }
}
