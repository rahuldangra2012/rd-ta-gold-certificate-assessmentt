﻿using OpenQA.Selenium;
using PageObjectModel.DataBase;
using PageObjectModel.GeneralUtils;
using PageObjectModel.UIFrameWork.UIUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PageObjectModel.UIFrameWork.Pages.LoginPage
{
    public partial class Login
    {
        AppSettings Config = FileHelper.ReadText("Application.json");
        public void GoToURl(IWebDriver driver)
        {
            driver.Navigate().GoToUrl(Config.Url);

        }
        public void EnterCredentials(IWebDriver driver)
        {
            Waits.WaitForElementToBeVisible(driver, UserName, TimeSpan.FromSeconds(10));
            var credentials = LoginDetails.LoginData.GetEnumerator();
            credentials.MoveNext();
            driver.FindElement(UserName).SendKeys(credentials.Current.Key);
            driver.FindElement(Password).SendKeys(credentials.Current.Value);

        }
        public void clickLogin(IWebDriver driver)
        {
            driver.FindElement(LoginButton).Click();
        }
    }
}
