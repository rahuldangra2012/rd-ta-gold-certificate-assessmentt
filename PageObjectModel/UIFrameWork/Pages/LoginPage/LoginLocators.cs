﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.UIFrameWork.Pages.LoginPage
{
    public partial class Login
    {
        private By UserName = By.Id("userName");
        private By Password = By.Id("password");
        private By LoginButton = By.Id("login");
    }
}
