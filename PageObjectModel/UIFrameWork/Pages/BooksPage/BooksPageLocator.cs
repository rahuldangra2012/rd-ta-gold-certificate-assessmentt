﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.UIFrameWork.Pages.BooksPage
{
    public partial class BooksPage
    {
        

        public static By Rows => By.XPath("//div[@class='rt-tr-group']");
        public static By Titles => By.XPath(".//div[contains(@class, 'action-buttons')]/span/a");
        public static By Authors => By.XPath(".//div[contains(@class, 'rt-td')][3]");
        public static By Publishers => By.XPath(".//div[contains(@class, 'rt-td')][4]");
    }
}
