﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageObjectModel.ApiFramework.Models.Response.LoginV1UserResponse;
using PageObjectModel.GeneralUtils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.UIFrameWork.Pages.BooksPage
{
    public partial class BooksPage
    {
        
        public List<Book> CreateBooks(IWebDriver driver)
        {
            AppSettings Config = FileHelper.ReadText("Application.json");

            driver.Navigate().GoToUrl(Config.BooksUrl);
            List<Book> books = new List<Book>();


            var bookElements = driver.FindElements(Rows);


            foreach (var bookElement in bookElements)
            {

                var author = bookElement.FindElement(Authors);
                var publisher = bookElement.FindElement(Publishers);

                if (author != null && publisher != null)
                {

                    string authors = author.Text;
                    string publishers = publisher.Text;
                    try
                    {
                        var title = bookElement.FindElement(Titles);
                        string titleText = title.Text;
                        books.Add(new Book { title = titleText, author = authors, publisher = publishers });
                    }
                    catch (NoSuchElementException ex)
                    {
                        Console.WriteLine($"Message{ex.Message} and Stack Trace{ex.StackTrace}");
                    }
                }

            }
            return books;
        
        }
    }
}
    
