﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Safari;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.UIFrameWork.UIUtils
{
    public static class Setup
    {
        public static IWebDriver _driver;
        public static IWebDriver GetDriver(string browserType)
        {
            if (_driver == null)
            {
                switch (browserType)
                {
                    case "Chrome":
                        _driver = new ChromeDriver();
                        break;

                    case "Safari":
                        _driver = new SafariDriver();
                        break;

                    case "Edge":
                        _driver = new EdgeDriver();
                        break;

                    default:
                        throw new ArgumentException("Unsupported Browser Type");
                }

            }

            return _driver;
        }

    }
}
