﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.Models.Response.LoginV1UserResponse
{
    public class ErrorModel
    {
        public int code { get; set; }
        public string message { get; set; }
    }
}
