﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.Models.Response.LoginV1UserResponse
{
    public class UserModel
    {
        public string userId { get; set; }
        public string username { get; set; }
        public List<Book> books { get; set; }
    }
}
