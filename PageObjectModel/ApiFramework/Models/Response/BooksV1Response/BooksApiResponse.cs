﻿using PageObjectModel.ApiFramework.Models.Response.LoginV1UserResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.Models.Response.BooksV1Response
{
    public class BooksApiResponse
    {
        public List<Book> books { get; set; }
    }
}
