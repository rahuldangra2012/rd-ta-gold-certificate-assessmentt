﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.Models.Request
{
    public class V1UserFactoryValidRequest
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
