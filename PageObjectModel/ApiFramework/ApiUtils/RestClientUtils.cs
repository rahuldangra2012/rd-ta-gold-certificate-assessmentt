﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.ApiUtils
{
    public static class RestClientUtils
    {
        public static RestClient CreateClient(string Url)
        {
            RestClient client = new RestClient(Url);
            return client;
        }
    }
}
