﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.ApiUtils
{
    public static class ApiOperations
    {
        public static RestResponse Get<T>(RestClient client, string resource) where T : class
        {
            var response = client.Execute
                           (
                              RestRequestUtils.CreateRequest(resource, Method.Get)
                           );
            return response;
        }

        public static RestResponse Put<T>(RestClient client, string resource, string payload) where T : class
        {
            var response = client.Execute(RestRequestUtils.CreateRequest(resource, Method.Put).AddBody(payload));
            return response;
        }
        public static RestResponse Post<T>(RestClient client, string resource, string payload) where T : class
        {
            var response = client.Execute(RestRequestUtils.CreateRequest(resource, Method.Post).AddBody(payload));
            return response;
        }

        public static RestResponse Delete(RestClient client, string resource)
        {
            var response = client.Execute(RestRequestUtils.CreateRequest(resource, Method.Delete));
            return response;

        }

        public static T Patch<T>(RestClient client, string resource, string payload) where T : class
        {
            var response = client.Execute(RestRequestUtils.CreateRequest(resource, Method.Patch).AddBody(payload));
            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
