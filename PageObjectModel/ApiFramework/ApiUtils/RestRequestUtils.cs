﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.ApiUtils
{
    public static  class RestRequestUtils
    {
        public static RestRequest CreateRequest(String resource, Method method)
        {
            RestRequest Request = new RestRequest(resource, method);
            return Request;
        }
    }
}
