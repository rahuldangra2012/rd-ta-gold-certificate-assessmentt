﻿using Newtonsoft.Json;
using PageObjectModel.ApiFramework.Models.Request;
using PageObjectModel.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.ApiUtils
{
    public static class CreatePostRequestBody
    {
   
        public static string CreateV1UserRequest()
        {
            var credentials = LoginDetails.LoginData.GetEnumerator();
            credentials.MoveNext();
            string username = credentials.Current.Key;
            string password = credentials.Current.Value;
            V1UserFactoryValidRequest request = new V1UserFactoryValidRequest
            {
                userName = username,
                password = password

            };

            return JsonConvert.SerializeObject(request);
        }
    }
}
