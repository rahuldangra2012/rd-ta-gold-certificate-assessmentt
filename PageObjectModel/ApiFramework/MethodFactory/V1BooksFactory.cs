﻿using PageObjectModel.ApiFramework.ApiUtils;
using PageObjectModel.ApiFramework.Models.Response.BooksV1Response;
using PageObjectModel.ApiFramework.Models.Response.LoginV1UserResponse;
using PageObjectModel.GeneralUtils;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.MethodFactory
{
    public class V1BooksFactory
    {
        AppSettings Config = FileHelper.ReadText("Application.json");
        public RestResponse GetBookData()
        {
            RestClient client = RestClientUtils.CreateClient(Config.Uri);
            string Resource = Config.BooksResource;
            return ApiOperations.Get<BooksApiResponse>(client, Resource);
        }
    }
}
