﻿using PageObjectModel.ApiFramework.ApiUtils;
using PageObjectModel.ApiFramework.Models.Response.LoginV1UserResponse;
using PageObjectModel.GeneralUtils;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObjectModel.ApiFramework.MethodFactory
{
    public class V1UserFactory
    {
        AppSettings Config = FileHelper.ReadText("Application.json");
        public RestResponse LoginCall()
        {
            RestClient client = RestClientUtils.CreateClient(Config.Uri);
            string Resource = Config.Resource;
            string payload = CreatePostRequestBody.CreateV1UserRequest();
            return ApiOperations.Post<ApiResponse>(client, Resource,payload);
        }
    }
}
