using BoDi;
using NUnit.Framework;
using OpenQA.Selenium.DevTools.V121.Profiler;
using PageObjectModel.DataBase;
using PageObjectModel.UIFrameWork.Pages.LoginPage;
using PageObjectModel.UIFrameWork.Pages.ProfilePage;
using System;
using System.ComponentModel;
using TechTalk.SpecFlow;

namespace RDAssessment.StepDefinitions
{
    [Binding]
    public class ApiAndUIIntegrationStepDefinitions:StepBase
    {
        Login Login;
        Profiles Profiles;
        public ApiAndUIIntegrationStepDefinitions(IObjectContainer container) : base(container)
        {
            Login = new Login();    
            Profiles= new Profiles();
            container.RegisterInstanceAs<Login>(Login);
            container.RegisterInstanceAs<Profiles>(Profiles);

        }


        [Given(@"I Go to url for the website")]
        public void GivenIGoToUrlForTheWebsite()
        {
            Login.GoToURl(Driver);
        }

        [When(@"I Enter username and password \{created from the previous API call}\.")]
        public void WhenIEnterUsernameAndPasswordCreatedFromThePreviousAPICall_()
        {
            Login.EnterCredentials(Driver);
        }

        [When(@"I willClick on the Login button\.")]
        public void WhenIWillClickOnTheLoginButton_()
        {
            Login.clickLogin(Driver);
        }

        [Then(@"I will Validate Username on the Books Dashboard page\.")]
        public void ThenIWillValidateUsernameOnTheBooksDashboardPage_()
        {
            String Name = Profiles.FindUserName(Driver);
            var credentials = LoginDetails.LoginData.GetEnumerator();
            credentials.MoveNext();
            Assert.That(Name, Is.EqualTo(credentials.Current.Key));
        }
    }
}
