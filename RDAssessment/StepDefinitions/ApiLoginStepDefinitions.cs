using Newtonsoft.Json;
using NUnit.Framework;
using PageObjectModel.ApiFramework.MethodFactory;
using PageObjectModel.ApiFramework.Models.Response.LoginV1UserResponse;
using PageObjectModel.DataBase;
using RestSharp;
using System;
using System.Net;
using TechTalk.SpecFlow;

namespace RDAssessment.StepDefinitions
{

    [Binding]
    public class ApiLoginStepDefinitions
    {
        private readonly V1UserFactory _userFactory = new V1UserFactory() ;
        private RestResponse response;



        [Given(@"I will Make an API call to the given url and  I will create a user login using the below Parameters")]
        public void GivenIWillMakeAnAPICallToTheGivenUrlAndIWillCreateAUserLoginUsingTheBelowParameters(Table table)
        {
            foreach (var row in table.Rows)
            {
                string username = row["UserName"];
                string password = row["Password"];
                LoginDetails.LoginData.Add(username, password);
            }
            response = _userFactory.LoginCall();
        }


        [Then(@"I will Validate StatusCode and response body")]
        public void ThenIWillValidateStatusCodeAndResponseBody()
        {
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
            ApiResponse DeserialisedResponse = JsonConvert.DeserializeObject<ApiResponse>(response.Content);
            var credentials = LoginDetails.LoginData.GetEnumerator();
            credentials.MoveNext();
            Assert.That(DeserialisedResponse.username, Is.EqualTo(credentials.Current.Key));
            Thread.Sleep(3000);

        }
    }
}
