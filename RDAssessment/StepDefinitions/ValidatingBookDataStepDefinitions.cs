using BoDi;
using Newtonsoft.Json;
using NUnit.Framework;
using PageObjectModel.ApiFramework.MethodFactory;
using PageObjectModel.ApiFramework.Models.Response.BooksV1Response;
using PageObjectModel.ApiFramework.Models.Response.LoginV1UserResponse;
using PageObjectModel.UIFrameWork.Pages.BooksPage;
using RestSharp;
using System;
using System.Net;
using TechTalk.SpecFlow;

namespace RDAssessment.StepDefinitions
{
    [Binding]
    public class ValidatingBookDataStepDefinitions:StepBase
    {
        private readonly V1BooksFactory _BooksFactory;
        private BooksApiResponse DeserialisedResponse;
        private readonly BooksPage booksPage;
        private List<Book> booksCollection = new  List<Book>();
        public ValidatingBookDataStepDefinitions(IObjectContainer container) : base(container) 
        {
            _BooksFactory = new V1BooksFactory();
            booksPage = new BooksPage();
        }
       
        [Given(@"Make an API call To The Books Uri and capture the details of all the books\.")]
        public void GivenMakeAnAPICallToTheBooksUriAndCaptureTheDetailsOfAllTheBooks_()
        {
            RestResponse response = _BooksFactory.GetBookData();
            DeserialisedResponse = JsonConvert.DeserializeObject<BooksApiResponse>(response.Content);

        }

        [Then(@"Validate the captured details i\.e\., Title, Author, Publisher from above response by launching the URL on WEB UI\.")]
        public void ThenValidateTheCapturedDetailsI_E_TitleAuthorPublisherFromAboveResponseByLaunchingTheURLOnWEBUI_()
        {
            booksCollection =  booksPage.CreateBooks(Driver);
            foreach (var book in DeserialisedResponse.books)
            {
                foreach(var book2 in booksCollection)
                {
                    if (book2.title==book.title) 
                    {
                        Assert.That(book.publisher, Is.EqualTo(book2.publisher));
                        Assert.That(book.author, Is.EqualTo(book2.author));
                        
                    }
                }
            }
           
        }
    }
}
