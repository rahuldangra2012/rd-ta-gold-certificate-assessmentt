﻿using BoDi;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDAssessment.StepDefinitions
{
    [Binding]
    public class StepBase
    {
        public readonly IObjectContainer Container;

        protected StepBase(IObjectContainer continer)
        {
            Container = continer;
        }

        protected IWebDriver Driver => Container.Resolve<IWebDriver>();
    }
}
