﻿using Allure.Net.Commons;
using BoDi;
using OpenQA.Selenium;
using PageObjectModel.UIFrameWork.UIUtils;
using TechTalk.SpecFlow;

namespace RDAssessment.Hooks
{
    [Binding]
    public sealed class Hooks
    {


        private readonly IObjectContainer _container;
       


        public Hooks(IObjectContainer container)
        {
            _container = container;
            
        }

        private static readonly AllureLifecycle allure = AllureLifecycle.Instance;
        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            allure.CleanupResultDirectory();     
        }

        [BeforeScenario("@LoginUser", "ValidationBookData")]
        public void BeforeScenarioWithTag()
        {
            IWebDriver _driver = Setup.GetDriver("Chrome");
            _container.RegisterInstanceAs(_driver);

        }


        [AfterScenario("ValidationBookData")] 
        public void AfterScenario()
        {
            IWebDriver _driver = _container.Resolve<IWebDriver>();
            _driver.Quit();
        }
    }
}