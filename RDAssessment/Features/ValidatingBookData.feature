﻿Feature: ValidatingBookData

This is to ensure that both Api and Ui fetch same Data

@ValidationBookData
Scenario: Validating Book Data
	Given Make an API call To The Books Uri and capture the details of all the books.
    Then  Validate the captured details i.e., Title, Author, Publisher from above response by launching the URL on WEB UI.
	
