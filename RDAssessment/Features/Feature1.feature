﻿Feature: Api and UI integration

To Perform Integration of Api and UI


@CreateUser
Scenario: Creating a Login Request
	Given I will Make an API call to the given url and  I will create a user login using the below Parameters
	| UserName | Password    |
	| Rahuls1   | Rahul@12345 |
	Then I will Validate StatusCode and response body

@LoginUser
Scenario: Validating from UI
Given  I Go to url for the website
When   I Enter username and password {created from the previous API call}.
And    I willClick on the Login button.
Then   I will Validate Username on the Books Dashboard page.